<?php

namespace App\Commands;

use App\Sites\Hoc247Net\QuizCrawler;
use Illuminate\Console\Scheduling\Schedule;
use LaravelZero\Framework\Commands\Command;

class CrawlHoc247Net extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'hoc247net {--id= : Id of quiz}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Crawl from hoc247.net';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $crawler = new QuizCrawler();
        for($id = 30000;  $id<40000; $id++){
            $crawler->process( $id );
            dump("====================");
        }
    }

    /**
     * Define the command's schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule $schedule
     * @return void
     */
    public function schedule(Schedule $schedule): void
    {
        // $schedule->command(static::class)->everyMinute();
    }
}
