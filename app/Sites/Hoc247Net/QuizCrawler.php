<?php
/**
 * Crawl quizzes with url example https://hoc247.net/cau-hoi-qua-trinh-do-thi-hoa-nuoc-ta-hien-nay-co-dac-diem-la--qid111368.html
 * User: hocvt
 * Date: 2019-10-31
 * Time: 16:38
 */

namespace App\Sites\Hoc247Net;


use App\Crawler\Browsers\Guzzle;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DomCrawler\Crawler;

class QuizCrawler {
    
    protected $prefix = "https://hoc247.net/cau-hoi--qid__id__.html";
    protected $client;
    
    /**
     * QuizCrawler constructor.
     */
    public function __construct() {
        $this->client = new Client( config( 'crawler.browsers.guzzle' ) );
    }
    
    public function process( $id ) {
        $url = $this->makeUrl( $id );
        if ( $url ) {
            $html = ( new Guzzle( $this->client ) )->getHtml( $url );
            $this->processHtml( $html );
//            if ( $crawled_content ) {
//                $crawled_content['url'] = $url;
//
//                return $crawled_content;
//            } else {
//                throw new \Exception( "Can not parse " . $url );
//            }
        } else {
            return false;
        }
    }
    
    protected function processHtml( $html ) {
        $crawler = new Crawler();
        $crawler->addHtmlContent( $html );
        $keywords = [];
        try {
            $keywords[] = trim( $crawler->filter( 'ul.tlmenu li.act' )->text() );
        } catch ( \Exception $ex ) {
            dump( $ex->getMessage() );
        }
        try {
            $keywords[] = trim( $crawler->filterXPath( '//strong()' )->text() );
        } catch ( \Exception $ex ) {
            dump( $ex->getMessage() );
        }
        try {
            $keywords[] = trim( $crawler->filterXPath( '//p/strong[text()="Chủ đề :"]/following-sibling::*' )->text() );
        } catch ( \Exception $ex ) {
            dump( $ex->getMessage() );
        }
        try {
            $keywords[] = trim( $crawler->filterXPath( '//p/strong[text()="Môn học:"]/following-sibling::*' )->text() );
        } catch ( \Exception $ex ) {
            dump( $ex->getMessage() );
        }
        $keywords = array_filter( $keywords );
        
        dump( 'KEYWORDS : ' . implode( ", ", $keywords ) );
        
        try {
            $source_title = trim( $crawler->filter( '.list-content-cauhoi .i-head .i-title' )->text() );
        } catch ( \Exception $ex ) {
            dump( $ex->getMessage() );
        }
        dump( "SOURCE TITLE : " . $source_title );
        
        try {
            $player = $crawler->filter( '#itvc20player li.lch' );
            
            $clear_count = 0;
            $content_html = '';
            $player->children()->each( function ( Crawler $child ) use (&$clear_count, &$content_html) {
                if($child->attr( 'class') == 'clear'){
                    $clear_count++;
                    return;
                }
                if($child->nodeName() == 'script'){
                    return;
                }
                if($clear_count > 0 && $clear_count < 3){
                    $content_html .= $child->html() . "\n";
                }
            });
            
        } catch ( \Exception $ex ) {
            dump( $ex->getMessage() );
        }
        dump( "CONTENT : \n" . strip_tags( $content_html ) );
    }
    
    protected function makeUrl( $id ) {
        $url = str_replace( "__id__", $id, $this->prefix );
        dump('Checking ' . $url);
        try {
            $response = $this->client->head( $url, [
                'timeout'         => 10,
                'allow_redirects' => false,
            ] );
            if ( count( $response->getHeader( 'Location' ) ) ) {
                return $response->getHeader( 'Location' )[0];
            } else {
                return false;
            }
        } catch ( RequestException $exception ) {
            if ( ! $exception->getResponse() ) {
                throw $exception;
            } else {
                return false;
            }
        }
    }
    
    
}